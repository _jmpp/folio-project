app.controller('CompetencesController', function() {
  var competences = this;

  competences.liste = [
    { libelle : 'HTML5', niveau : 100 },
    { libelle : 'CSS3', niveau : 100 },
    { libelle : 'JavaScript', niveau : 100 },
    { libelle : 'AngularJS', niveau : 50 },
    { libelle : 'PHP', niveau : 40 },
    { libelle : 'MySQL', niveau : 30 }
  ];
});