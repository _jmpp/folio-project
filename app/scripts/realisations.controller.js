app.controller('RealisationsController', function() {
  var realisations = this;

  realisations.liste = [
    { titre : 'Site web', description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus tenetur dicta modi fuga eaque, quidem eveniet sunt perspiciatis laudantium illum laborum similique magni tempore dolorem. Quisquam, ipsam voluptates hic beatae!', image : '300x150.png' },
    { titre : 'Maquette', description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus tenetur dicta modi fuga eaque, quidem eveniet sunt perspiciatis laudantium illum laborum similique magni tempore dolorem. Quisquam, ipsam voluptates hic beatae!', image : '300x150.png' },
    { titre : 'Site web', description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus tenetur dicta modi fuga eaque, quidem eveniet sunt perspiciatis laudantium illum laborum similique magni tempore dolorem. Quisquam, ipsam voluptates hic beatae!', image : '300x150.png' },
    { titre : 'Jeu vidéo', description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus tenetur dicta modi fuga eaque, quidem eveniet sunt perspiciatis laudantium illum laborum similique magni tempore dolorem. Quisquam, ipsam voluptates hic beatae!', image : '300x150.png' },
    { titre : 'Site web', description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus tenetur dicta modi fuga eaque, quidem eveniet sunt perspiciatis laudantium illum laborum similique magni tempore dolorem. Quisquam, ipsam voluptates hic beatae!', image : '300x150.png' },
    { titre : 'Etude de projet', description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus tenetur dicta modi fuga eaque, quidem eveniet sunt perspiciatis laudantium illum laborum similique magni tempore dolorem. Quisquam, ipsam voluptates hic beatae!', image : '300x150.png' },
  ];
});