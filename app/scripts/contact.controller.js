app.controller('ContactController', function() {
  var contact = this;

  contact.erreurs = [];

  contact.sendMessage = function(form) {
    contact.estInvalide = form.$invalid;
    contact.erreurs.length = 0;

    console.log(form);

    if (form.nom.$invalid)
      contact.erreurs.push('Le champs "nom" est invalide');
    if (form.email.$invalid)
      contact.erreurs.push('Le champs "email" est invalide');
    if (form.message.$invalid)
      contact.erreurs.push('Le champs "message" est invalide');

  }
});