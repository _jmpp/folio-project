var app = angular.module('Folio', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/home');

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'partials/home.html',
      controller: 'HomeController',
      controllerAs: 'home'
    })
    .state('competences', {
      url: '/competences',
      templateUrl: 'partials/competences.html',
      controller: 'CompetencesController',
      controllerAs: 'competences'
    })
    .state('realisations', {
      url: '/realisations',
      templateUrl: 'partials/realisations.html',
      controller: 'RealisationsController',
      controllerAs: 'realisations'
    })
    .state('contact', {
      url: '/contact',
      templateUrl: 'partials/contact.html',
      controller: 'ContactController',
      controllerAs: 'contact'
    });

})